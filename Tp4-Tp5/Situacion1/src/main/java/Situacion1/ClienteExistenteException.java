package Situacion1;

public class ClienteExistenteException extends RuntimeException{
    public ClienteExistenteException() {
        super("El cliente ya existe en la base de datos, si desea puede modificar el existente.");
    }
}
