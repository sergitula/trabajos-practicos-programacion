package Situacion1;

public class DomicilioEdificio extends Domicilio {
    private String calle;
    private Integer piso;
    private String departamento;

    public DomicilioEdificio(Integer numeroCasa, String calle, Integer piso, String departamento ){
        super(numeroCasa);
        this.calle= calle;
        this.piso= piso;
        this.departamento= departamento;
    }

    public String getCalle() {
        return calle;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Integer getPiso() {
        return piso;
    }

    public void setPiso(Integer piso) {
        this.piso = piso;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    @Override
    public String toString() {
        return "DomicilioEdificio [N� Casa: " + super.getClass()+", Calle: " + calle + ", Dpto.:" + departamento + ", Piso:" + piso +"]";
    }
    
}
