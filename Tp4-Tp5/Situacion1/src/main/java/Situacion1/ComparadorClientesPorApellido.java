/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Situacion1;

import java.util.Comparator;

/**
 *
 * @author Sergio
 */
public class ComparadorClientesPorApellido implements Comparator<Cliente>{

    @Override
    public int compare(Cliente cliente1, Cliente cliente2) {
        return cliente1.getApellido().compareTo(cliente2.getApellido());
    }
    
}
