/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Situacion1.gui;

import Situacion1.Cliente;
import Situacion1.ClienteInexistenteException;
import Situacion1.Estudio;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sergio
 */
public class VentanaEliminarModificar extends javax.swing.JFrame {

    /**
     * Creates new form VentanaEliminarModificar
     */
    DefaultTableModel modelo = new DefaultTableModel();
    String matriz[][]= new  String [7][2];
    
    private Estudio estudio;
    public VentanaEliminarModificar(Estudio estudio) {
        super("Eliminar y Modificar");
        this.estudio = estudio;
        initComponents();
//        limpiarLista();
        cargarDatos();
        this.setLocationRelativeTo(null);
        
    }
    
    public void cargarDatos(){
        modelo.addColumn("Caracteristicas");
        modelo.addColumn("Datos");
            
        
        matriz[0][0]= "Nombre"; 
        matriz[1][0]= "Apellido";
        matriz[2][0]= "Documento";
        matriz[3][0]= "Asunto";
        matriz[4][0]= "Correo";
        matriz[5][0]= "Telefono";
        matriz[6][0]= "Domicilio";
        
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            matriz,
            new String [] {
                "Caracteristicas"
            }
        ));
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelBotones = new javax.swing.JPanel();
        btn_Eliminar = new javax.swing.JButton();
        btn_Modificar = new javax.swing.JButton();
        btn_Cancelar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jlblIngreseDocumento = new javax.swing.JLabel();
        jtxtf_Documento = new javax.swing.JTextField();
        btn_Buscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btn_Eliminar.setText("Eliminar");
        btn_Eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_EliminarActionPerformed(evt);
            }
        });
        jPanelBotones.add(btn_Eliminar);

        btn_Modificar.setText("Modificar");
        btn_Modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ModificarActionPerformed(evt);
            }
        });
        jPanelBotones.add(btn_Modificar);

        btn_Cancelar.setText("Cancelar");
        btn_Cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CancelarActionPerformed(evt);
            }
        });
        jPanelBotones.add(btn_Cancelar);

        jlblIngreseDocumento.setText("Ingrese documento");

        jtxtf_Documento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtf_DocumentoActionPerformed(evt);
            }
        });

        btn_Buscar.setText("Buscar");
        btn_Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_BuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlblIngreseDocumento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtxtf_Documento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_Buscar)
                .addGap(16, 16, 16))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblIngreseDocumento)
                    .addComponent(jtxtf_Documento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_Buscar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Caracteristicas", "Datos"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelBotones, javax.swing.GroupLayout.DEFAULT_SIZE, 596, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 596, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtxtf_DocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtf_DocumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtf_DocumentoActionPerformed

    private void btn_CancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CancelarActionPerformed
        // TODO add your handling code here:
        dispose();
        this.setVisible(false);
    }//GEN-LAST:event_btn_CancelarActionPerformed

    private void btn_EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_EliminarActionPerformed
        // TODO add your handling code here:
        //Si la segunda columna esta vacia el documento no fue buscado ...
        if(jTable1.getColumnCount() == 1){
            JOptionPane.showMessageDialog(null, "No se puede eliminar sin buscar el elemento.", "Advertencia", JOptionPane.WARNING_MESSAGE);
        }else{
            //El try-catch no es del todo necesario ya que no ocurrira la excepcion.(tiene que pasar antes por el boton buscar)
            try {
                estudio.eliminarCliente(buscarCliente());
                JOptionPane.showMessageDialog(rootPane,"Cliente eliminado exitosamente.");
            } catch (ClienteInexistenteException ex) {
                Logger.getLogger(VentanaEliminarModificar.class.getName()).log(Level.SEVERE, null, ex);
            }
         
        }

    }//GEN-LAST:event_btn_EliminarActionPerformed
    
    public Cliente buscarCliente()throws ClienteInexistenteException{
        Integer documento = Integer.parseInt(this.jtxtf_Documento.getText().trim());
        
        Cliente cliente = estudio.getCliente(documento);
        return cliente;
    }

    
    private void btn_BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_BuscarActionPerformed
        Integer documento = Integer.parseInt(this.jtxtf_Documento.getText().trim());
        try {
            //Cliente clienteEncontrado = estudio.getCliente(documento);
            Cliente clienteEncontrado = buscarCliente();
            
            matriz[0][1]= clienteEncontrado.getNombre();
            matriz[1][1]= clienteEncontrado.getApellido();
            matriz[2][1]= clienteEncontrado.getDocumento().toString();
            matriz[3][1]= clienteEncontrado.getAsunto();
            matriz[4][1]= clienteEncontrado.getCorreo();
            matriz[5][1]= clienteEncontrado.getTelefono().toString();
            matriz[6][1]= clienteEncontrado.getDomicilio().toString();
            
            jTable1.setModel(new javax.swing.table.DefaultTableModel(
                matriz,
                new String [] {
                    "Caracteristicas", "Datos"
                }
            ));
            

        } catch (ClienteInexistenteException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
        }
    }//GEN-LAST:event_btn_BuscarActionPerformed

    private void btn_ModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ModificarActionPerformed
        // TODO add your handling code here:
        if(jTable1.getColumnCount() == 1){
            JOptionPane.showMessageDialog(null, "No se puede eliminar sin buscar el elemento.", "Advertencia", JOptionPane.WARNING_MESSAGE);
        }else{
            //El try-catch no es del todo necesario ya que no ocurrira la excepcion.(tiene que pasar antes por el boton buscar)
            try {
                estudio.eliminarCliente(buscarCliente());
                Integer banderaLLamada = 2;
                
                this.setVisible(false);
                
                VentanaAgregar modificar = new VentanaAgregar(estudio, banderaLLamada);
                modificar.setVisible(true);
                
                
            } catch (ClienteInexistenteException ex) {
                JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
            }
         
        }
    }//GEN-LAST:event_btn_ModificarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Buscar;
    private javax.swing.JButton btn_Cancelar;
    private javax.swing.JButton btn_Eliminar;
    private javax.swing.JButton btn_Modificar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelBotones;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel jlblIngreseDocumento;
    private javax.swing.JTextField jtxtf_Documento;
    // End of variables declaration//GEN-END:variables
}
