/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Situacion1.gui;

import Situacion1.Estudio;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sergio
 */
public class VentanaMostrar extends javax.swing.JFrame {
    
    DefaultTableModel modelo = new DefaultTableModel();
    private Estudio estudio;
    /**
     * Creates new form VentanaMostrar
     */
    public VentanaMostrar(Estudio estudio) {
        super("Mostrar datos");
        this.estudio = estudio;
        initComponents();
        cargarDatos();
        this.setLocationRelativeTo(null);
        
        
    }

    public void cargarDatos(){

        //insertamos los datos.        
        String matriz[][]= new  String [estudio.cantidadClientes()][7];
            
        for(int i = 0; i< estudio.cantidadClientes(); i++){
            matriz[i][0]= estudio.getClientes().get(i).getNombre();
            matriz[i][1]= estudio.getClientes().get(i).getApellido();
            matriz[i][2]= estudio.getClientes().get(i).getDocumento().toString();
            matriz[i][3]= estudio.getClientes().get(i).getAsunto();
            matriz[i][4]= estudio.getClientes().get(i).getCorreo();
            matriz[i][5]= estudio.getClientes().get(i).getTelefono().toString();
            matriz[i][6]= estudio.getClientes().get(i).getDomicilio().toString();            
        }
            
        jtabla_datos.setModel(new javax.swing.table.DefaultTableModel(
        matriz,
        new String [] {
        "Nombre","Apellido", "Documento", "Asunto", "Correo", "Telefono", "Domicilio"
        }
        ));
        jtabla_datos.getColumnModel().getColumn(0).setPreferredWidth(20);
        jtabla_datos.getColumnModel().getColumn(1).setPreferredWidth(20);
        jtabla_datos.getColumnModel().getColumn(2).setPreferredWidth(10);
        jtabla_datos.getColumnModel().getColumn(3).setPreferredWidth(100);
        jtabla_datos.getColumnModel().getColumn(4).setPreferredWidth(50);
        jtabla_datos.getColumnModel().getColumn(5).setPreferredWidth(10);
        jtabla_datos.getColumnModel().getColumn(6).setPreferredWidth(120);
   
    }
    
    
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_MostrarListado = new javax.swing.JLabel();
        jComboBox_Ordenar = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtabla_datos = new javax.swing.JTable();
        btnVolver = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lbl_MostrarListado.setText("Mostrar listado ordenado por:");

        jComboBox_Ordenar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "Documento", "Apellido" }));
        jComboBox_Ordenar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_OrdenarActionPerformed(evt);
            }
        });

        jtabla_datos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Nombre", "Apellido", "Documento", "Asunto", "Correo", "Telefono", "Domicilio"
            }
        ));
        jtabla_datos.setEnabled(false);
        jtabla_datos.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jtabla_datosAncestorAdded(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jScrollPane1.setViewportView(jtabla_datos);
        if (jtabla_datos.getColumnModel().getColumnCount() > 0) {
            jtabla_datos.getColumnModel().getColumn(2).setPreferredWidth(40);
            jtabla_datos.getColumnModel().getColumn(5).setPreferredWidth(40);
        }

        btnVolver.setText("Volver");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbl_MostrarListado)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox_Ordenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 974, Short.MAX_VALUE))
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnVolver)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_MostrarListado)
                    .addComponent(jComboBox_Ordenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnVolver)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtabla_datosAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jtabla_datosAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_jtabla_datosAncestorAdded

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        // TODO add your handling code here:
        dispose();
        setVisible(false);
    }//GEN-LAST:event_btnVolverActionPerformed

    private void jComboBox_OrdenarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_OrdenarActionPerformed

        if(this.jComboBox_Ordenar.getSelectedItem().equals("Documento")){
            estudio.getInformacionOrdenadaNumeroDeDocumento();
            cargarDatos();
        }
        if(this.jComboBox_Ordenar.getSelectedItem().equals("Apellido")){
            estudio.getInformacionOrdenadaApellido();
            cargarDatos();
        }
    }//GEN-LAST:event_jComboBox_OrdenarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnVolver;
    private javax.swing.JComboBox<String> jComboBox_Ordenar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtabla_datos;
    private javax.swing.JLabel lbl_MostrarListado;
    // End of variables declaration//GEN-END:variables
}
