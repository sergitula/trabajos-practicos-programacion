/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Situacion1;

/**
 *
 * @author Sergio
 */
public class ClienteSinDomicilioException extends Exception {

    public ClienteSinDomicilioException() {
        super("El cliente no tiene un domicilio asignado.");
    }
    
}
