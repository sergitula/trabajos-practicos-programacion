package Situacion2;

import java.math.BigDecimal;
import javax.swing.JOptionPane;

public class HerramientaElectrica extends Herramienta {
    private BigDecimal consumo;

    //constructor
    public HerramientaElectrica(String codigo, String nombre,String funcionalidad, BigDecimal precio,Integer cantidad, BigDecimal consumo) {
        super(codigo, nombre,funcionalidad,cantidad, precio);
        this.consumo= consumo;
    }


    //setters y getters
    public BigDecimal getConsumo() {
        return consumo;
    }

    public void setConsumo(BigDecimal consumo) {
        this.consumo = consumo;
    }

    @Override
    public String getDescripcion() {
        return super.getNombre()+" H.E.  -  $"+ super.getPrecio()+" x Und.  -  " + super.getCantidad()+" Unidades" +" - Func.: " + super.getFuncionalidad()+ " - " +consumo +" Consumo.";
    }

    @Override
    public BigDecimal getPrecioTotal() {
        return (super.getPrecio()).multiply(new BigDecimal(super.getCantidad()));
    }

    @Override
    public String getCodigo(){
        return super.getCodigo();
    }
    
       @Override
    public void setCantidad(Integer cantidad) {
        super.setCantidad(cantidad);
    }

    @Override
    public BigDecimal getPrecioUnitario() {
        return super.getPrecio();
    }

      @Override
    public Integer getCantidadDisponible(Integer cantidadVenta) {
        Integer venta = super.getCantidad()-cantidadVenta;
        if(venta < 0){
            try {
                throw new StockInsuficienteException();
            } catch (StockInsuficienteException ex) {
                JOptionPane.showMessageDialog(null,ex.getMessage(), "Alerta",0);
            }
            
        }
        return venta; 
    }
}
