package Situacion2;

import java.math.BigDecimal;

public interface ItemVenta {
    BigDecimal getPrecioTotal();
    String getCodigo();
    String getDescripcion();
    BigDecimal getPrecioUnitario();
    Integer getCantidadDisponible(Integer cantidadVenta);
    void setCantidad(Integer cantidad);

}
