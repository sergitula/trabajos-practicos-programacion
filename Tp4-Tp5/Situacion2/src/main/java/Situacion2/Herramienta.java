package Situacion2;

import java.math.BigDecimal;

public abstract class Herramienta implements ItemVenta {
    private String codigo;
    private String nombre;
    private String funcionalidad;
    private Integer cantidad;
    private BigDecimal precio;

    // ---Constructor---
    public Herramienta(String codigo, String nombre,String funcionalidad,Integer cantidad, BigDecimal precio) {
        this.codigo= codigo;
        this.nombre= nombre;
        this.funcionalidad= funcionalidad;
        this.cantidad= cantidad;
        this.precio= precio;
    }
    //---getters y setters---

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }
    public String getFuncionalidad(){
        return funcionalidad;
    }
    public void setFuncionalidad(String funcionalidad){
        this.funcionalidad= funcionalidad;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    
    
}


