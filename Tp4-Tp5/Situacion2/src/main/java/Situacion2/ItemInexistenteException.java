/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Situacion2;

/**
 *
 * @author Sergio
 */
public class ItemInexistenteException extends Exception{
    public ItemInexistenteException(){
        super("El item no existe en la base de datos.");
    }
}
