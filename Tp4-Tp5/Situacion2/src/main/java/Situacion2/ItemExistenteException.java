/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Situacion2;

/**
 *
 * @author Sergio
 */
public class ItemExistenteException extends Exception{
    public ItemExistenteException(){
        super("¡Articulo ya existente! Si desea agregar mas cantidad modifique el articulo existente.");
    }
}
