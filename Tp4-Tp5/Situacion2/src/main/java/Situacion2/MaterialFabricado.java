package Situacion2;

import java.math.BigDecimal;
import javax.swing.JOptionPane;


public class MaterialFabricado extends Material implements ItemVenta {
    private Integer cantidad;
    //---Constructor---
    public MaterialFabricado(String codigo, String nombre, BigDecimal precio, Integer cantidad) {
        super(codigo, nombre, precio);
        this.cantidad= cantidad;
    }

    // ---setters y getters---
    public Integer getCantidad() {
        return cantidad;
    }

    @Override
    public String getDescripcion() {
        return super.getNombre()+" M.F.  -  $"+ super.getPrecio()+" x Und.  -  " + cantidad+" Unidades";
    }

    @Override
    public BigDecimal getPrecioTotal() {
        return (super.getPrecio()).multiply(new BigDecimal(cantidad));
    }

    @Override
    public String getCodigo(){
        return super.getCodigo();
    }
    

    @Override
    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public BigDecimal getPrecioUnitario() {
        return super.getPrecio();
    }

    @Override
    public Integer getCantidadDisponible(Integer cantidadVenta) {
        Integer venta = cantidad-cantidadVenta;
        if(venta < 0){
            try {
                throw new StockInsuficienteException();
            } catch (StockInsuficienteException ex) {
                JOptionPane.showMessageDialog(null,ex.getMessage(), "Alerta",0);
            }
            
        }
        return venta; 
    }
    

  



}
