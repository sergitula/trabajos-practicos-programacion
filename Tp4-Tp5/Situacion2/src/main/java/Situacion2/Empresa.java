
package Situacion2;

import java.util.ArrayList;
import java.util.List;

public class Empresa {
    private String nombre;
    private List<OrdenVenta> ordenes;
    public Empresa(String nombre){
        this.nombre= nombre;
        ordenes = new ArrayList<OrdenVenta>();
    }
    
    
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public List<OrdenVenta> getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(List<OrdenVenta> ordenes) {
        this.ordenes = ordenes;
    }
   
    

    
    
    //tiene que tener un  numero de orden y no se tiene que repetir.
    public void agregarOrden(OrdenVenta orden){
        ordenes.add(orden);
    }

    public int cantidadOrdenes(){
        return ordenes.size();
    }

    
}
