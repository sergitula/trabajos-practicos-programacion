package Situacion2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrdenVenta {
    private String numeroOrden;
    List<ItemVenta> orden;
    List<ItemVenta> auxiliar;

    // constructor
    public OrdenVenta() {
        orden = new ArrayList<ItemVenta>();
        // guardara items que se quieran vender..
        auxiliar = new ArrayList<ItemVenta>();
    }

    //Getters y Setters
    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }


    public void agregarItemVenta(ItemVenta item) throws ItemExistenteException {
        for(ItemVenta var: orden){
            if(var.getCodigo().equals(item.getCodigo())){
                throw new ItemExistenteException();
            }
        }
        orden.add(item);
    }
    
    public void eliminarItemVenta(String codigo) throws ItemInexistenteException{
        ItemVenta itemEncontrado= getItem(codigo);
        orden.remove(itemEncontrado);
    }
    
    public void modificarItemVenta(ItemVenta nuevoItem) throws ItemInexistenteException{
        ItemVenta itemEncontrado;
        itemEncontrado = getItem(nuevoItem.getCodigo());
        orden.remove(itemEncontrado);
        orden.add(nuevoItem);
    }
    
    public ItemVenta getItem(String codigo) throws ItemInexistenteException{
        ItemVenta itemEncontrado= null;
        for(ItemVenta var: orden){
            if(var.getCodigo().equals(codigo)){
                itemEncontrado = var;
                break;
            }
        }
        if(itemEncontrado == null){
            throw new ItemInexistenteException();
        }
        return itemEncontrado;
    }
    

    public int cantidadDeItems(){
        return orden.size();
    }

    public List<ItemVenta> mostrarItemsVenta() {
        return orden;
    }

    //Ordenamiento
    public void mostrarOrdenadosPorDescripcion(){
        Collections.sort(orden, new ComparadorItemsPorDescripcion()); // Faltar
    }

    public void mostrarOrdenadorPorPrecio(){
         Collections.sort(orden, new ComparadorItemsPorPrecio()); //Falta
    }
    
    //Creando arrays auxiliares..
    public List<ItemVenta> mostrarAuxiliarItemsVenta(){
        return auxiliar;
    }
    public void agregarAuxiliarItem(ItemVenta item) throws AuxiliarItemExistenteException{
        for(ItemVenta var :  auxiliar){
            if(var.getCodigo().equals(item.getCodigo())){
                throw new AuxiliarItemExistenteException();
            }
        }
        auxiliar.add(item);
    }
    public void elimiarAuxiliarItem(ItemVenta item){
        auxiliar.remove(item);
    }
    public int AuxiliarCantidadDeItems(){
        return auxiliar.size();
    }
    
    
    
    
   
    public void mostrarOrden(){
        for(ItemVenta item : orden){
            System.out.println(item);
        }
    }

}
