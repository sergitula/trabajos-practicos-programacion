/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Situacion2.gui;


import Situacion2.AuxiliarItemExistenteException;
import Situacion2.ItemVenta;
import Situacion2.OrdenVenta;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.swing.DefaultListModel;

import javax.swing.JOptionPane;


/**
 *
 * @author Sergio
 */
public class FormularOrdenVenta extends javax.swing.JFrame implements WindowListener{


    
    private Integer indiceLista = null;
    DefaultListModel modeloLista = new DefaultListModel();
    ArrayList cantidades = new ArrayList (); 
    private OrdenVenta orden;
  
    public FormularOrdenVenta(OrdenVenta orden) {
        super("Venta de Articulos");
        this.orden = orden;
        initComponents();
        cargarLista();
        this.setLocationRelativeTo(null);
    }

   
    public void cargarLista(){
        jListArticulos.setModel(modeloLista);
        String [] mostrar= new String[orden.cantidadDeItems()];
        for(int i=0; i< orden.cantidadDeItems();i++){
           mostrar[i]= orden.mostrarItemsVenta().get(i).getDescripcion();
           modeloLista.addElement(mostrar[i]);
        }
    }
    
    public void cargarTabla(){
        String matriz[][] = new String [orden.mostrarAuxiliarItemsVenta().size()][4];
        for(int i=0;  i< orden.mostrarAuxiliarItemsVenta().size(); i++){
            matriz[i][0] = orden.mostrarAuxiliarItemsVenta().get(i).getCodigo();
            matriz[i][1]= orden.mostrarAuxiliarItemsVenta().get(i).getDescripcion();
            matriz[i][2]= cantidades.get(i).toString();

            Integer cantidad = Integer.parseInt(cantidades.get(i).toString());
            //BigDecimal precioUnitario = orden.mostrarAuxiliarItemsVenta().get(i).getPrecioUnitario();
            //matriz[i][3] = precioUnitario.multiply(new BigDecimal(cantidad)).toString();
            matriz[i][3] = (orden.mostrarAuxiliarItemsVenta().get(i).getPrecioUnitario()).multiply(new BigDecimal(cantidad)).toString();
        }
        jTableVenta.setModel(new javax.swing.table.DefaultTableModel(
            matriz,
            new String [] {
                "Codigo", "Descripci�n", "Cantidad", "SubTotal"
            }
        ));
       
        jTableVenta.getColumnModel().getColumn(1).setPreferredWidth(250);
 
        precioTotal();

    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableVenta = new javax.swing.JTable();
        lblTotal = new javax.swing.JLabel();
        jtxtf_Total = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        btnFacturar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lblCantidad = new javax.swing.JLabel();
        jtxtf_Cantidad = new javax.swing.JTextField();
        btnAgregarItem = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListArticulos = new javax.swing.JList<>();
        btnEliminarItem = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTableVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Codigo", "Descripci�n", "Cantidad", "SubTotal"
            }
        ));
        jScrollPane1.setViewportView(jTableVenta);
        if (jTableVenta.getColumnModel().getColumnCount() > 0) {
            jTableVenta.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTableVenta.getColumnModel().getColumn(1).setPreferredWidth(150);
            jTableVenta.getColumnModel().getColumn(2).setPreferredWidth(20);
            jTableVenta.getColumnModel().getColumn(3).setPreferredWidth(30);
        }

        lblTotal.setText("Total a pagar $");

        jtxtf_Total.setEditable(false);

        btnFacturar.setText("Facturar");
        btnFacturar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFacturarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(512, Short.MAX_VALUE)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnFacturar)
                .addGap(6, 6, 6))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFacturar)
                    .addComponent(btnCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblCantidad.setText("Cantidad");

        btnAgregarItem.setText("Agregar Item");
        btnAgregarItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarItemActionPerformed(evt);
            }
        });

        jListArticulos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListArticulosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jListArticulos);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(btnAgregarItem))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtxtf_Cantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCantidad))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblCantidad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jtxtf_Cantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAgregarItem)
                .addGap(46, 46, 46))
        );

        btnEliminarItem.setText("Eliminar Item");
        btnEliminarItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarItemActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(btnEliminarItem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTotal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jtxtf_Total, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtxtf_Total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotal)
                    .addComponent(btnEliminarItem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    private void btnAgregarItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarItemActionPerformed

        if(jtxtf_Cantidad.getText().isEmpty()){
               JOptionPane.showMessageDialog(null, "Por favor, registre la cantidad en el campo correspondiente.", "Advertencia", JOptionPane.WARNING_MESSAGE);
        }else{
            //int filaSeleccionada = jListArticulos.getSelectedIndex();
            //System.out.println(filaSeleccionada);
            if(jListArticulos.getSelectedIndex() >=0){
                Integer cantidad = Integer.parseInt(jtxtf_Cantidad.getText().trim());
                Integer cantidadRestante = orden.mostrarItemsVenta().get(jListArticulos.getSelectedIndex()).getCantidadDisponible(cantidad);
                if(cantidadRestante >= 0 ){
                    try {
                        orden.agregarAuxiliarItem(orden.mostrarItemsVenta().get(jListArticulos.getSelectedIndex()));
                    } catch (AuxiliarItemExistenteException ex) {
                        JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
                    }
                    cantidades.add(cantidad);
                }
                jtxtf_Cantidad.setText("");
                cargarTabla();
                precioTotal();
            }else{
                JOptionPane.showMessageDialog(null, "Seleccione el articulo en la lista para a�adir", "Advertencia", JOptionPane.WARNING_MESSAGE);
            }
        }

 
    }//GEN-LAST:event_btnAgregarItemActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        orden.mostrarAuxiliarItemsVenta().clear();
        cantidades.clear();
        dispose();
        this.setVisible(false);
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void jListArticulosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListArticulosMouseClicked

    }//GEN-LAST:event_jListArticulosMouseClicked

    private void btnEliminarItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarItemActionPerformed
        // TODO add your handling code here:
        int filaSeleccionada = jTableVenta.getSelectedRow(); 
        if(filaSeleccionada>=0){
            //Eliminamos la lista y la cantidad.
            orden.elimiarAuxiliarItem(orden.mostrarAuxiliarItemsVenta().get(filaSeleccionada));
            cantidades.remove(cantidades.get(filaSeleccionada));
            cargarTabla();
            precioTotal();
        }else{
            JOptionPane.showMessageDialog(rootPane, "Seleccione el articulo a borrar en la tabla.", "Alerta",0);
        }
        
    }//GEN-LAST:event_btnEliminarItemActionPerformed

    private void btnFacturarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacturarActionPerformed
        // TODO add your handling code here:
        
        if(orden.AuxiliarCantidadDeItems() >0){
            Integer indice = 0;
            for(ItemVenta item : orden.mostrarAuxiliarItemsVenta()){
                
                Integer indice2=0;
                for(ItemVenta item2: orden.mostrarItemsVenta()){
                    if((orden.mostrarAuxiliarItemsVenta().get(indice).getCodigo()).equals(orden.mostrarItemsVenta().get(indice2).getCodigo())){
                        Integer cantidad = Integer.parseInt(cantidades.get(indice).toString());
                        Integer cantidadRestante = orden.mostrarItemsVenta().get(indice2).getCantidadDisponible(cantidad);
                        if(cantidadRestante >= 0){
                             orden.mostrarItemsVenta().get(indice2).setCantidad(cantidadRestante);
                         }
                    }
                    indice2 ++;
                }
                indice ++;
            }
            JOptionPane.showMessageDialog(rootPane, "Facturaci�n exitosa"); 
            Integer confirmar = JOptionPane.showConfirmDialog(null, "�Mostrar orden facturada?", "Confirmar salida", JOptionPane. YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if(confirmar == 0){
                
                OrdenFacturada mostrarFactura = new OrdenFacturada(orden, cantidades);
                //Seteamos para otra operacion.
                orden.mostrarAuxiliarItemsVenta().clear();
                cantidades.clear();
                this.setVisible(false);
                mostrarFactura.setVisible(true);
            }else{
                //reiniamos los auxiliares y mostramos la tabla vacia.
                orden.mostrarAuxiliarItemsVenta().clear();
                cantidades.clear();
                cargarTabla();
            }
           
        }else{
            JOptionPane.showMessageDialog(rootPane, "No hay articulos a facturar."); 
        }
        
        
    }//GEN-LAST:event_btnFacturarActionPerformed


    public void precioTotal(){
        BigDecimal total = new BigDecimal(00);
        Integer indice=0;
        for(ItemVenta var: orden.mostrarAuxiliarItemsVenta()){
            Integer cantidad = Integer.parseInt(cantidades.get(indice).toString());
            total= total.add(orden.mostrarAuxiliarItemsVenta().get(indice).getPrecioUnitario().multiply(new BigDecimal(cantidad)));
            indice++;
        }

        jtxtf_Total.setText(total.setScale(2).toString());
        
    }
    
 
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarItem;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminarItem;
    private javax.swing.JButton btnFacturar;
    private javax.swing.JList<String> jListArticulos;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableVenta;
    private javax.swing.JTextField jtxtf_Cantidad;
    private javax.swing.JTextField jtxtf_Total;
    private javax.swing.JLabel lblCantidad;
    private javax.swing.JLabel lblTotal;
    // End of variables declaration//GEN-END:variables

    @Override
    public void windowOpened(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosed(WindowEvent e) {
        orden.mostrarAuxiliarItemsVenta().clear();
        cantidades.clear();
        
        dispose();
        this.setVisible(false);
        
    }

    @Override
    public void windowIconified(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowActivated(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosing(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
