package Situacion2;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Proveedor {
    private Integer cuit;
    private String razonSocial;
    private BigDecimal precio;
    private ArrayList<MateriaPrima> materiaprima;

    // constructor
    public Proveedor(Integer cuit, String razonSocial, BigDecimal precio) {
        this.cuit = cuit;
        this.razonSocial = razonSocial;
        this.precio = precio;
    }

    // getters y setters
    public Integer getCuit() {
        return cuit;
    }
    public void setCuit(Integer cuit) {
        this.cuit = cuit;
    }
    
    
    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public ArrayList<MateriaPrima> getMateriaprima() {
        return materiaprima;
    }

    public void setMateriaprima(ArrayList<MateriaPrima> materiaprima) {
        this.materiaprima = materiaprima;
    }


    public void agregarMateriaPrima(MateriaPrima materiaP){
        materiaprima.add(materiaP);
    }


    public void venderMateriaPrima(MateriaPrima materiaP){
    //     for(MateriaPrima var: materiaprima){
    //         if(var.getCodigo().equals(materiaP.getCodigo())){
    //             if(var.getCantidadMateriaPrima().equals(materiaP.getCantidadMateriaPrima())){
    //                 materiaprima.remove(materiaP);
    //             }
    //         }
    //     }
    }


}
