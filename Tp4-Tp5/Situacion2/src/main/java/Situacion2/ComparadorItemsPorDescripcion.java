/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Situacion2;

import java.util.Comparator;

/**
 *
 * @author Sergio
 */
public class ComparadorItemsPorDescripcion implements Comparator<ItemVenta>{
    
    @Override
    public int compare(ItemVenta item1, ItemVenta item2) {
        return item1.getDescripcion().compareTo(item2.getDescripcion());
    }
    
}
