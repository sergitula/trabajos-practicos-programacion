package Situacion2;

import java.math.BigDecimal;
import javax.swing.JOptionPane;

public class HerramientaManual extends Herramienta {
    public HerramientaManual(String codigo, String nombre,String funcionalidad, BigDecimal precio, Integer cantidad) {
        super(codigo, nombre,funcionalidad,cantidad, precio);
    }


    @Override
    public void setCantidad(Integer cantidad) {
        super.setCantidad(cantidad);
    }

    
    @Override
    public BigDecimal getPrecioTotal() {
        return (super.getPrecio()).multiply(new BigDecimal(super.getCantidad()));
    }
    @Override
    public String getCodigo(){
        return super.getCodigo();
    }
    
    @Override
    public String getDescripcion() {
        return super.getNombre()+" H.M.  -  $"+ super.getPrecio()+" x Und.  -  " + super.getCantidad()+" Unidades" + "  -Func.: " + super.getFuncionalidad();
    }

    @Override
    public BigDecimal getPrecioUnitario() {
        return super.getPrecio();
    }

    @Override
    public Integer getCantidadDisponible(Integer cantidadVenta) {
        Integer venta = super.getCantidad()-cantidadVenta;
        if(venta < 0){
            try {
                throw new StockInsuficienteException();
            } catch (StockInsuficienteException ex) {
                JOptionPane.showMessageDialog(null,ex.getMessage(), "Alerta",0);
            }
            
        }
        return venta; 
    }
    
 
}
