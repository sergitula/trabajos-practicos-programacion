package Situacion2;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class OrdenTest {
    OrdenVenta orden;
    MaterialFabricado item1;
    HerramientaManual item2;
    HerramientaElectrica item3;
    BigDecimal precio1, precio2, precio3, consumo;
    @Before
    public void init(){
        orden = new OrdenVenta();
        precio1 = new BigDecimal(10);
        item1 = new MaterialFabricado("COD1","TORNILLOS", precio1, 50);
        precio2 = new BigDecimal(700);
        item2  = new HerramientaManual("COD2", "Martillo","S/especificar", precio2, 2);
        consumo = new BigDecimal(100);
        precio3 = new BigDecimal(5000);
        item3 = new HerramientaElectrica("COD3", "Taladro","S/especificar", precio3, 1, consumo);
    }

        //Comprobar listas vacias
    @Test
    public void testComprobarOrdenVacia(){
        assertEquals(0, orden.cantidadDeItems());

    }
    public void testComprobarOrdenAxuiliarVacia(){
        assertEquals(0,orden.AuxiliarCantidadDeItems());
    }


    //Comprobar ingreso de items.
    @Test
    public void testAgregarItemsAOrden() throws ItemExistenteException {
        orden.agregarItemVenta(item1);
        orden.agregarItemVenta(item2);
        assertEquals(2, orden.cantidadDeItems());
    }
    @Test
    public void testAgregarItemAlAuxiliar() throws AuxiliarItemExistenteException {
        orden.agregarAuxiliarItem(item1);
        orden.agregarAuxiliarItem(item3);
        assertEquals(2, orden.AuxiliarCantidadDeItems());
    }    
    @Test (expected = ItemExistenteException.class)
    public void testAgregarItemRepetidoAOrden() throws ItemExistenteException {
        orden.agregarItemVenta(item1);
        orden.agregarItemVenta(item1);        
    }
    @Test (expected = AuxiliarItemExistenteException.class)
    public void testAgregarItemRepetidoAlAuxiliar() throws AuxiliarItemExistenteException {
        orden.agregarAuxiliarItem(item1);
        orden.agregarAuxiliarItem(item1);
    }

        //Comprobar eliminar
    @Test
    public void eliminarItemEnOrden() throws ItemExistenteException, ItemInexistenteException {
        orden.agregarItemVenta(item3);
        orden.eliminarItemVenta("COD3");
        assertEquals(0, orden.cantidadDeItems());
    }

    @Test (expected = ItemInexistenteException.class)
    public void eliminarItemInexistente() throws ItemExistenteException, ItemInexistenteException {
        orden.agregarItemVenta(item3);
        orden.eliminarItemVenta("COD4");
    }

    @Test
    public void eliminarItemAuxiliar() throws ItemExistenteException, AuxiliarItemExistenteException {
        orden.agregarAuxiliarItem(item3);
        orden.elimiarAuxiliarItem(item3);
        assertEquals(0, orden.cantidadDeItems());
    }

    
}
