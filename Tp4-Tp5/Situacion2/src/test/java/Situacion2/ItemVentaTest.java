package Situacion2;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class ItemVentaTest {

    OrdenVenta orden;
    MaterialFabricado item1;
    HerramientaManual item2;
    HerramientaElectrica item3;
    BigDecimal precio1, precio2, precio3, consumo;
    @Before
    public void init(){
        orden = new OrdenVenta();
        precio1 = new BigDecimal(10);
        item1 = new MaterialFabricado("COD1","TORNILLOS", precio1, 50);
        precio2 = new BigDecimal(700);
        item2  = new HerramientaManual("COD2", "Martillo","S/especificar", precio2, 5);
        consumo = new BigDecimal(100);
        precio3 = new BigDecimal(5000);
        item3 = new HerramientaElectrica("COD3", "Taladro","S/especificar", precio3, 10, consumo);
    }


    @Test 
    public void testGetPrecioTotal() throws ItemExistenteException {
        orden.agregarItemVenta(item1);
        BigDecimal precioTotal= orden.mostrarItemsVenta().get(0).getPrecioTotal();
        BigDecimal totalEsperado = new BigDecimal(500);
        assertEquals(totalEsperado, precioTotal);
    }

    @Test
    public void testGetCodigo() throws ItemExistenteException {
        orden.agregarItemVenta(item2);
        String codigo = orden.mostrarItemsVenta().get(0).getCodigo();
        String codigoEsperado = "COD2";
        assertEquals(codigoEsperado,codigo);
    }

    @Test
    public void testGetCantidadDisponeble() throws ItemExistenteException {
        orden.agregarItemVenta(item3);
        Integer cantidad = orden.mostrarItemsVenta().get(0).getCantidadDisponible(1);
        Integer cantidadEsperada = 9;
        assertEquals(cantidadEsperada, cantidad);
    }

    @Test 
    public void testGetPrecioUnitario() throws ItemExistenteException {
        orden.agregarItemVenta(item3);
        BigDecimal precioUnitario = orden.mostrarItemsVenta().get(0).getPrecioUnitario();
        BigDecimal precioEsperado = new BigDecimal(5000);
        assertEquals(precioEsperado, precioUnitario);
    }

    @Test 
    public void testGetDescipcion() throws ItemExistenteException {
        orden.agregarItemVenta(item1);
        String descripcion = orden.mostrarItemsVenta().get(0).getDescripcion();
        String descripcionEsperada = "TORNILLOS M.F.  -  $10 x Und.  -  50 Unidades";
        assertEquals(descripcionEsperada, descripcion);
    }

}
